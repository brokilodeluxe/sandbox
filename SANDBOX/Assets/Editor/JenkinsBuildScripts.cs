﻿using System;
using UnityEditor;

public class JenkinsEditorScript
{
    
    [MenuItem("BuildScripts/Windows Game Client")]
    public static void BuildGameClient()
    {
        Console.WriteLine("win build started");
        string[] levels = new string[] { "Assets/Scenes/SampleScene.unity" };

        BuildPlayerOptions bpo = new BuildPlayerOptions();
        bpo.scenes = levels;
        bpo.locationPathName = "Build/windows/SANDBOX.exe";
        bpo.target = BuildTarget.StandaloneWindows;
        bpo.options = BuildOptions.None;
        BuildPipeline.BuildPlayer(bpo);
        Console.WriteLine("win build end"); ;
    }
}
